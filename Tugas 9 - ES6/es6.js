/// 1. Mengubah fungsi menjadi fungsi arrow

// const golden = function goldenFunction(){
//     console.log("this is golden!!")
// }
    
// golden()
console.log("=Soal 1=")
console.log("")
let golden = () => {   
    console.log("this is golden!!")  
}

golden();

// 2. Sederhanakan menjadi Object literal di ES6
// const newFunction = function literal(firstName, lastName){
//     return {
//         firstName: firstName,
//         lastName: lastName,
//         fullName: function(){
//         console.log(firstName + " " + lastName)
//             return
//         }
//     }
// }
//     //Driver Code
//     newFunction("William", "Imoh").fullName()
console.log("=Soal 2=")
console.log("")

let newFunction = (firstName,lastName) =>({
    firstName: firstName,
    lastName: lastName,
    fullName : () => {
       return  console.log(firstName + " " + lastName)
    }
})

newFunction("William", "Imoh").fullName()

// 3. Destructuring

// const newObject = {
//     firstName: "Harry",
//     lastName: "Potter Holt",
//     destination: "Hogwarts React Conf",
//     occupation: "Deve-wizard Avocado",
//     spell: "Vimulus Renderus!!!"
//     }

//     const firstName = newObject.firstName;
//     const lastName = newObject.lastName;
//     const destination = newObject.destination;
//     const occupation = newObject.occupation;
    
//     console.log(firstName);
//     console.log(lastName);
//     console.log(destination);
//     console.log(occupation);

console.log("=Soal 3=")
console.log("")

    let newObject = {
        firstName: "Harry",
        lastName: "Potter Holt",
        destination: "Hogwarts React Conf",
        occupation: "Deve-wizard Avocado",
        spell: "Vimulus Renderus!!!"
        }
    
     const{firstName, lastName, destination, occupation, spell}   = newObject;
     
     console.log(firstName);
     console.log(lastName);
     console.log(destination);
     console.log(occupation);
     console.log(spell);

//  4. Array Spreading
// const west = ["Will", "Chris", "Sam", "Holly"]
// const east = ["Gill", "Brian", "Noel", "Maggie"]
// const combined = west.concat(east)
// //Driver Code
// console.log(combined)

console.log("=Soal 4=")
console.log("")

let west = ["Will", "Chris", "Sam", "Holly"]
let east = ["Gill", "Brian", "Noel", "Maggie"]
let combined = [...west, ...east]
console.log(combined)

// 5. Template Literals
// const planet = "earth"
// const view = "glass"
// var before = 'Lorem ' + view + 'dolor sit amet, ' +
// 'consectetur adipiscing elit,' + planet + 'do eiusmod tempor ' +
// 'incididunt ut labore et dolore magna aliqua. Ut enim' +
// ' ad minim veniam'
// // Driver Code
// console.log(before)

console.log("=Soal 5=")
console.log("")

let planet = 'earth'
let view = 'glass'
let before = `Lorem ${view}dolor sit amet, consectetur adipiscing elit,${planet}do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam`
// Driver Code
console.log(before)