import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View} from 'react-native';

// import Component from './Latihan/Component/Component';
// import YoutubeUI from './Tugas/Tugas12/App';
// import TextTrain from './Latihan/Component/Text';
// import ImageTrain from './Latihan/Component/Image';
// import ScrollViewTrain from './Latihan/Component/ScrollView';
import FlatListTrain from './Latihan/Component/FlatList';
// import TouchableOpacityTrain from './Latihan/Component/TouchableOpacity';
import TextInputTrain from './Latihan/Component/TextInput';

export default function App() {
  return (
    // <TextInputTrain />
    // <TouchableOpacityTrain />
    <FlatListTrain />
    // <ScrollViewTrain />
    // <ImageTrain />
    // <TextTrain />
    // <YoutubeUI />
    // <Component />

      // <View style={styles.container}>
      //   <Text>Open up App.js to start working on your app!</Text>
      //   <StatusBar style="auto" />
      // </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
