//// EKSPRESI CLASS 
// Tidak diberi nama
// var Car = class {
//     constructor(brand, factory) {
//      this.brand = brand
//      this.factory = factory
//      }
// }
    
//     console.log(Car.name) // Car

//     // Diberi nama
// var Car = class Car2 {
//     constructor(brand, factory) {
//      this.brand = brand
//       this.factory = factory
//     }
// }
    
//     console.log(Car.name) // Car2

//// METHOD
// class Car {
//     constructor(brand) {
//         this.carname = brand;
//     }
//     present() {
//         return "I have a " + this.carname;
//     }
// }
    
// mycar = new Car("Ford");
// console.log(mycar.present()) // I have a Ford

// class Car {
//     constructor(brand) {
//      this.carname = brand;
//     }
//     present(x) {
//      return x + ", I have a " + this.carname;
//     }
// }
//     mycar = new Car("Ford");
//     console.log(mycar.present("Hello"));


//// STATIC METHOD
// class Car {
//     constructor(brand) {
//     this.carname = brand;
//     }
//     static hello() {
//     return "Hello!!";
//     }
//     }
//     mycar = new Car("Ford");
//     // memanggil 'hello()' pada class Car:
//     console.log(Car.hello());
//     // dan tidak bisa pada 'mycar':
//     // console.log(mycar.hello());
//     // jika menggunakan sintaks tersebut akan memunculkan error.


//// INHERITANCE
// class Car {
//     constructor(brand) {
//         this.carname = brand;
//     }
//     present() {
//         return 'I have a ' + this.carname;
//     }
// }
//     class Model extends Car {
//         constructor(brand, mod) {
//             super(brand); // memanggil constructor di atasnya
//             this.model = mod;
//         }
//         show() {
//             return this.present() + ', it is a ' + this.model;
//         }
//     }
    
//     mycar = new Model("Ford", "Mustang");
//     console.log(mycar.show());

// GETTERS DAN SETTERS
// class Car {
//     constructor(brand) {
//         this.carname = brand;
//     }
//     get cnam() {
//         return this.carname;
//     }
//     set cnam(x) {
//         this.carname = x;
//     }
// }
//     mycar = new Car("Ford");
//     console.log(mycar.cnam); // Ford
//     // getter cnam digunakan tanpa "()"

    
class Car {
    constructor(brand) {
        this._carname = brand;
    }
    get carname() {
        return this._carname;
    }
    set carname(x) {
        this._carname = x;
    }
}
    mycar = new Car("Ford");
    mycar.carname = "Volvo"; // memanggil setter, mengubah Ford menjadi Volvo
    console.log(mycar.carname); // Volvo

    // kalau data dari property diolah dulu sebelum di 
    // tampilkan maka gunakan getter dan jika data ingin di olah dulu 
    // sebelum di assign ke property maka gunakan setter

