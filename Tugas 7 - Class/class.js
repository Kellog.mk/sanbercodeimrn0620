class Animal {
    // Code class di sini
    constructor(name){
        this.name = name;
        this.legs = 4;
        this.cold_blooded = false;
    }
}

var sheep = new Animal("shaun");
console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false

// Code class Ape dan class Frog di sini
class Ape extends Animal{
    constructor(name){
        super(name);
    }

    yell(){
        console.log("Auoo");
    }
}

class Frog extends Animal {
    constructor(name){
        super(name);
    }

    jump(){
        console.log("hop hop");
    }
}
var sungokong = new Ape("kera sakti") //test
sungokong.yell() // "Auooo"
var kodok = new Frog("buduk")
kodok.jump() // "hop hop"

// function Clock({ template }) {
//     var timer;
    
//     function render() {
//         var date = new Date();
//         var hours = date.getHours();
//         if (hours < 10) hours = '0' + hours;
        
//         var mins = date.getMinutes();
//         if (mins < 10) mins = '0' + mins;
    
//         var secs = date.getSeconds();
//         if (secs < 10) secs = '0' + secs;
    
//         var output = template
//         .replace('h', hours)
//         .replace('m', mins)
//         .replace('s', secs);
    
//         console.log(output);
//     }
    
//     this.stop = function() {
//         clearInterval(timer);
//     };
    
//     this.start = function() {
//         render();
//         timer = setInterval(render, 1000);
//     };
// }

class Clock{
    //Code di sini
    constructor({template}){
        this.template = template;
    }

    render() {
        var date = new Date();
        var hours = date.getHours();
        var mins = date.getMinutes();
        var secs = date.getSeconds();

        if (hours < 10) hours = '0' + hours;
        if (mins < 10) mins = '0' + mins;
        if (secs < 10) secs = '0' + secs;

        var output = this.template.replace('h',hours).replace('m',mins).replace('s',secs);
        console.log(output);
    }

    stop() { 
        clearInterval(this.timer);
    }

    start() {
        this.render();
        //Pilih salah satu
        
        //Pakai this alias
        // var that;
        // this.timer = setInterval(function(){
        //     that.render();
        // },1000)

        // Atau pakai arrow function (es6)
        this.timer = setInterval(() => this.render(),1000);

        // //Atau pakai bind
        // this.timer = setInterval(this.render.bind(this),1000);
        
    }
}

var clock = new Clock({template: 'h:m:s'});
clock.start();