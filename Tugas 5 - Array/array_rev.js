//Soal No. 1 (Range)

/*function range(startNum,finishNum){
var output = [];
var i = 0;


    if (startNum === undefined || finishNum === undefined) {
     return -1;
    } else {
      if(startNum < finishNum){
       for (i = startNum; i <= finishNum; i++){
        output.push(i);   
       }
       return output; 
     } else if (startNum > finishNum){
        for (i = startNum; i >= finishNum; i-- ){
            output.push(i);   
        }
        return output;
     } else {
      return -1;  
     }
    }
    
}

console.log("==Soal No. 1 (Range)==");
console.log("");

console.log(range(1,10));
console.log(range(1));
console.log(range(11,18));
console.log(range(54,50));
console.log(range());

console.log("");
console.log("==Soal No. 2 (Range with Step)==");
console.log("");
// Soal No. 2 (Range with Step)

function rangeWithStep(startNum,finishNum,step){
    var output = [];
    var i = 0;
    
    
        if (startNum === undefined || finishNum === undefined) {
         return -1;
        } else {
          if(startNum < finishNum){
           for (i = startNum; i <= finishNum; i+=step){
            output.push(i);   
           }
           return output; 
         } else if (startNum > finishNum){
            for (i = startNum; i >= finishNum; i-=step ){
                output.push(i);   
            }
            return output;
         } else {
          return -1;  
         }
        }
        
    }
    
console.log(rangeWithStep(1,10,2));
console.log(rangeWithStep(11,23,3));
console.log(rangeWithStep(5,2,1));
console.log(rangeWithStep(29,2,4));
    
console.log("");
console.log("==Soal No. 3 (Sum of Range)==");
console.log("");

// Soal No. 3 (Sum of Range)
function sum(startNum,finishNum,step){
    var output = [];
    var i = 0;
    var myTotal = 0;
    
    
        if (startNum === undefined || finishNum === undefined) {
         if (startNum !== undefined){
          return startNum;
         } else {return 0};
        } else {
          if (step === undefined) {
           step = 1; 
          } else {
           step = step;  
          }

          if(startNum < finishNum){
           for (i = startNum; i <= finishNum; i+=step){
            output.push(i);   
           }
           
           for (i=0; i<output.length;i++){
             myTotal += output[i];
           } 

           return myTotal; 
         } else if (startNum > finishNum){
            for (i = startNum; i >= finishNum; i-=step ){
                output.push(i);   
            }
            
            for (i=0; i<output.length;i++){
                myTotal += output[i];
            } 
   
              return myTotal;
         } else {
          return -1;  
         }
         return 0;
        }
        
    }

console.log(sum(1,10));
console.log(sum(5,50,2));
console.log(sum(15,10));
console.log(sum(20,10,2));
console.log(sum(1));
console.log(sum());

console.log("");
console.log("==Soal No. 4 (Array Multidimensi)==");
console.log("");

// Soal No. 4 (Array Multidimensi)
function dataHandling (masukan){

    for (let i = 0; i < masukan.length; i++) {
        var innerArrayLength = masukan[i].length;
        for (let j = 0; j < innerArrayLength; j++) {
            if (j==0) {
              console.log('Nomor ID: ' + input[i][j]); 
            } else if (j==1) {
              console.log('Nama Lengkap: ' + input[i][j]);   
            } else if (j==2){
              console.log('TTL: ' + input[i][j]);  
            } else if (j==3){
              console.log('Hobi: ' + input[i][j]);  
            } else {
              console.log("");  
            }           
        }
    }
}

var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
    ]

dataHandling(input);

console.log("");
console.log("==Soal No. 5 (Balik Kata)==");
console.log("");

// Soal No. 5 (Balik Kata)

function balikKata (kata){
    let balikan = "";    
    for (var i = kata.length - 1; i >= 0; i--){        
      balikan += kata[i];
    }    
    return balikan;

}

console.log(balikKata("Kasur Rusak"));
console.log(balikKata("SanberCode"));
console.log(balikKata("Haji Ijah"));
console.log(balikKata("racecar"));
console.log(balikKata("I am Sanbers"));*/

console.log("");
console.log("==Soal No. 6 (Metode Array)==");
console.log("");

// Soal No. 6 (Metode Array)
function dataHandling2 (masukan){

masukan.splice(1,1,"Roman Alamsyah Elsharawy");
masukan.splice(2,1,"Provinsi Bandar Lampung");
masukan.splice(4,1,"Pria","SMA Internasional Metro");

console.log(masukan);

var tanggalLahir = masukan[3].split("/");
var bulan =+tanggalLahir[1];

switch(bulan) {
    case 1: { console.log(`Januari`); break; }
    case 2: { console.log(`Februari`); break; }
    case 3: { console.log(`Maret`); break; }
    case 4: { console.log(`April`); break; }
    case 5: { console.log(`Mei`); break; }
    case 6: { console.log(`Juni`); break; }
    case 7: { console.log(`Juli`); break; }
    case 8: { console.log(`Agustus`); break; }
    case 9: { console.log(`September`); break; }
    case 10: { console.log(`Oktober`); break; }
    case 11: { console.log(`November`); break; }
    case 12: { console.log(`Desember`); break; }
    default: { console.log('Bulan tidak valid'); }
  }    

  var tanggalLahir_sort = tanggalLahir.sort((a,b)=> b-a);
  console.log(tanggalLahir_sort);
  
  console.log(masukan[3].split("/").join("-"));
  console.log(masukan[1].slice(0,15));
  
}

var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
dataHandling2(input);