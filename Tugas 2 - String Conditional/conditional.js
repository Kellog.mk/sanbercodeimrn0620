// If - else

var nama = "John";
var peran = "Penyihir"

if (nama == "" && peran == "") {
 console.log("Nama harus diisi!");
}

if (nama != "" && peran == "") {
 console.log("`Halo ${nama}, Pilih peranmu untuk memulai game!`");
}

console.log (`Selamat datang di Dunia Werewolf, ${nama}`);  

        if (peran == "Penyihir"){       
         console.log (`Halo ${peran} ${nama}, kamu dapat melihat siapa yang menjadi werewolf!`); 
        } else if  (peran == "Guard"){
         console.log (`Halo ${peran} ${nama}, kamu akan membantu melindungi temanmu dari serangan werewolf!`);       
        } else if (peran == "Werewolf") {
         console.log (`Halo ${peran} ${nama}, Kamu akan memakan mangsa setiap malam!`);              
        } else {
         console.log (`Halo ${nama}, peran ${peran} ini tidak ada!`);     
         console.log (`Pilih Penyihir, Guard atau Werewolf!`);     
        }

console.log ("");     

// Tugas conditional

var hari  = 21;
var bulan = 12;
var tahun = 1945;

if (hari < 1 || hari > 31) {
         console.log(`Isi dengan angka antara 1 - 31`);
         process.exit();
        }

if (tahun < 1900 || hari > 2200) {
         console.log(`Isi dengan angka antara 1900 - 2200`);
         process.exit();
        }

switch(bulan) {
case 1: { console.log(`${hari} Januari ${tahun}`); break; }
case 2: { console.log(`${hari} Februari ${tahun}`); break; }
case 3: { console.log(`${hari} Maret ${tahun}`); break; }
case 4: { console.log(`${hari} April ${tahun}`); break; }
case 5: { console.log(`${hari} Mei ${tahun}`); break; }
case 6: { console.log(`${hari} Juni ${tahun}`); break; }
case 7: { console.log(`${hari} Juli ${tahun}`); break; }
case 8: { console.log(`${hari} Agustus ${tahun}`); break; }
case 9: { console.log(`${hari} September ${tahun}`); break; }
case 10: { console.log(`${hari} Oktober ${tahun}`); break; }
case 11: { console.log(`${hari} November ${tahun}`); break; }
case 12: { console.log(`${hari} Desember ${tahun}`); break; }
default: { console.log('Bulan tidak valid'); }}

process.exit();