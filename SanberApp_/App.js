import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

// import YoutubeUI from './Tugas/Tugas12/App';
// import Quiz3 from './Tugas/Quiz3/index';

export default function App() {
  return (
    // <Quiz3 />
    // <YoutubeUI />
    <View style={styles.container}>
      <Text>Open up App.js to start working on your app! Bebek</Text>
      <StatusBar style="auto" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
