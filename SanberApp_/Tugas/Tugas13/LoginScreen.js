import React, { Component } from "react";
import { StyleSheet, View, Image, Text } from "react-native";

function LoginScreen(props) {
  return (
    <View style={styles.container}>
      <Image
        source={require("./assets/images/logo1.png")}
        resizeMode="contain"
        style={styles.image}
      ></Image>
      <Text style={styles.login}>Login</Text>
      <View style={styles.group1}>
        <Text style={styles.password}>Password</Text>
        <View style={styles.rect1}></View>
      </View>
      <View style={styles.group2}>
        <Text style={styles.usernameEmail}>Username/Email</Text>
        <View style={styles.rect2}></View>
      </View>
      <View style={styles.group3}>
        <View style={styles.rect3}>
          <Text style={styles.masuk1}>Masuk ?</Text>
        </View>
      </View>
      <View style={styles.group4}>
        <View style={styles.rect4}>
          <Text style={styles.daftar1}>Daftar</Text>
        </View>
      </View>
      <Text style={styles.atau1}>atau</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  image: {
    width: 375,
    height: 116,
    backgroundColor: "rgba(62,198,255,1)",
    marginTop: 59,
    alignSelf: "center"
  },
  login: {
    fontFamily: "roboto-regular",
    color: "rgba(0,51,102,1)",
    height: 28,
    width: 60,
    fontSize: 24,
    marginTop: 60,
    marginLeft: 158
  },
  group1: {
    width: 294,
    height: 71,
    marginTop: 127,
    marginLeft: 41
  },
  password: {
    fontFamily: "roboto-regular",
    color: "rgba(0,51,102,1)",
    height: 19,
    width: 73,
    fontSize: 16
  },
  rect1: {
    width: 294,
    height: 48,
    backgroundColor: "rgba(255,255,255,1)",
    borderWidth: 1,
    borderColor: "rgba(0,51,102,1)",
    shadowColor: "rgba(0,0,0,1)",
    shadowOffset: {
      width: 3,
      height: 3
    },
    elevation: 5,
    shadowOpacity: 1,
    shadowRadius: 0,
    marginTop: 5
  },
  group2: {
    width: 294,
    height: 71,
    marginTop: -158,
    marginLeft: 41
  },
  usernameEmail: {
    fontFamily: "roboto-regular",
    color: "rgba(0,51,102,1)",
    height: 19,
    width: 127,
    fontSize: 16
  },
  rect2: {
    width: 294,
    height: 48,
    backgroundColor: "rgba(255,255,255,1)",
    borderWidth: 1,
    borderColor: "rgba(0,51,102,1)",
    shadowColor: "rgba(0,0,0,1)",
    shadowOffset: {
      width: 3,
      height: 3
    },
    elevation: 5,
    shadowOpacity: 1,
    shadowRadius: 0,
    marginTop: 5
  },
  group3: {
    width: 140,
    height: 40,
    borderRadius: 16,
    marginTop: 119,
    marginLeft: 118
  },
  rect3: {
    width: 140,
    height: 40,
    backgroundColor: "rgba(62,198,255,1)",
    borderWidth: 1,
    borderColor: "#000000",
    borderRadius: 15
  },
  masuk1: {
    fontFamily: "roboto-regular",
    color: "rgba(255,255,255,1)",
    height: 28,
    width: 90,
    fontSize: 24,
    marginTop: 6,
    marginLeft: 25
  },
  group4: {
    width: 140,
    height: 40,
    borderRadius: 16,
    marginTop: 60,
    marginLeft: 118
  },
  rect4: {
    width: 140,
    height: 40,
    backgroundColor: "rgba(0,51,102,1)",
    borderWidth: 1,
    borderColor: "#000000",
    borderRadius: 15
  },
  daftar1: {
    fontFamily: "roboto-regular",
    color: "rgba(255,255,255,1)",
    height: 28,
    width: 72,
    fontSize: 24,
    marginTop: 6,
    marginLeft: 35
  },
  atau1: {
    fontFamily: "roboto-regular",
    color: "rgba(62,198,255,1)",
    height: 28,
    width: 48,
    fontSize: 24,
    marginTop: -84,
    marginLeft: 164
  }
});

export default LoginScreen;
