import React, { Component } from "react";
import { StyleSheet, View, Text } from "react-native";
import IoniconsIcon from "react-native-vector-icons/Ionicons";
import FontAwesomeIcon from "react-native-vector-icons/FontAwesome";
import Svg, { Ellipse } from "react-native-svg";

function AboutScreen(props) {
  return (
    <View style={styles.container}>
      <View style={styles.group9}>
        <View style={styles.rect7}>
          <Text style={styles.hubungiSaya}>Hubungi Saya</Text>
          <View style={styles.rect8}></View>
          <View style={styles.icon4Row}>
            <IoniconsIcon
              name="logo-facebook"
              style={styles.icon4}
            ></IoniconsIcon>
            <Text style={styles.mukhlisHanafi72}>mukhlis.hanafi</Text>
          </View>
          <View style={styles.icon5Row}>
            <IoniconsIcon
              name="logo-instagram"
              style={styles.icon5}
            ></IoniconsIcon>
            <Text style={styles.mukhlisHanafi73}>@mukhlis.hanafi</Text>
          </View>
          <View style={styles.icon6Row}>
            <IoniconsIcon
              name="logo-twitter"
              style={styles.icon6}
            ></IoniconsIcon>
            <Text style={styles.mukhlis74}>@mukhlis</Text>
          </View>
        </View>
      </View>
      <View style={styles.group8}>
        <View style={styles.rect5}>
          <Text style={styles.portofolio}>Portofolio</Text>
          <View style={styles.rect6}></View>
          <View style={styles.group6Row}>
            <View style={styles.group6}>
              <FontAwesomeIcon
                name="gitlab"
                style={styles.icon2}
              ></FontAwesomeIcon>
              <Text style={styles.mukhlish}>@mukhlish</Text>
            </View>
            <View style={styles.group7}>
              <IoniconsIcon
                name="logo-github"
                style={styles.icon7}
              ></IoniconsIcon>
              <Text style={styles.mukhlisH8}>@mukhlis-h</Text>
            </View>
          </View>
        </View>
      </View>
      <View style={styles.group5}>
        <View style={styles.ellipseStack}>
          <Svg viewBox="0 0 200.35 200" style={styles.ellipse}>
            <Ellipse
              stroke="rgba(230, 230, 230,1)"
              strokeWidth={0}
              fill="rgba(239,239,239,1)"
              cx={100}
              cy={100}
              rx={100}
              ry={100}
            ></Ellipse>
          </Svg>
          <IoniconsIcon name="md-person" style={styles.icon}></IoniconsIcon>
        </View>
      </View>
      <Text style={styles.tentangSaya}>Tentang Saya</Text>
      <Text style={styles.mukhlisHanafi71}>Mukhlis Hanafi</Text>
      <Text style={styles.reactNativeDeveloper}>React Native Developer</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff"
  },
  group9: {
    width: 359,
    height: 251,
    marginTop: 561,
    marginLeft: 8
  },
  rect7: {
    width: 359,
    height: 251,
    backgroundColor: "rgba(239,239,239,1)",
    borderRadius: 16
  },
  hubungiSaya: {
    fontFamily: "roboto-regular",
    color: "rgba(0,51,102,1)",
    width: 111,
    height: 21,
    fontSize: 18,
    marginTop: 9,
    marginLeft: 8
  },
  rect8: {
    width: 343,
    height: 1,
    backgroundColor: "rgba(0,51,102,1)",
    marginTop: 8,
    marginLeft: 8
  },
  icon4: {
    color: "rgba(62,198,255,1)",
    fontSize: 45,
    width: 34,
    height: 49
  },
  mukhlisHanafi72: {
    fontFamily: "roboto-700",
    color: "rgba(0,51,102,1)",
    fontSize: 16,
    width: 107,
    height: 19,
    marginLeft: 27,
    marginTop: 13
  },
  icon4Row: {
    height: 49,
    flexDirection: "row",
    marginTop: 15,
    marginLeft: 87,
    marginRight: 104
  },
  icon5: {
    color: "rgba(62,198,255,1)",
    fontSize: 45,
    width: 34,
    height: 49
  },
  mukhlisHanafi73: {
    fontFamily: "roboto-700",
    color: "rgba(0,51,102,1)",
    fontSize: 16,
    width: 124,
    height: 19,
    marginLeft: 27,
    marginTop: 13
  },
  icon5Row: {
    height: 49,
    flexDirection: "row",
    marginTop: 20,
    marginLeft: 87,
    marginRight: 87
  },
  icon6: {
    color: "rgba(62,198,255,1)",
    fontSize: 40,
    width: 36,
    height: 44
  },
  mukhlis74: {
    fontFamily: "roboto-700",
    color: "rgba(0,51,102,1)",
    fontSize: 16,
    width: 81,
    height: 19,
    marginLeft: 25,
    marginTop: 13
  },
  icon6Row: {
    height: 44,
    flexDirection: "row",
    marginTop: 20,
    marginLeft: 87,
    marginRight: 130
  },
  group8: {
    width: 359,
    height: 140,
    marginTop: -394,
    marginLeft: 8
  },
  rect5: {
    width: 359,
    height: 140,
    backgroundColor: "rgba(239,239,239,1)",
    borderRadius: 16
  },
  portofolio: {
    fontFamily: "roboto-regular",
    color: "rgba(0,51,102,1)",
    height: 21,
    width: 80,
    fontSize: 18,
    marginLeft: 8
  },
  rect6: {
    width: 343,
    height: 1,
    backgroundColor: "rgba(0,51,102,1)",
    marginTop: 9,
    marginLeft: 8
  },
  group6: {
    width: 81,
    height: 72
  },
  icon2: {
    color: "rgba(62,198,255,1)",
    fontSize: 45,
    width: 45,
    height: 45,
    marginLeft: 18
  },
  mukhlish: {
    fontFamily: "roboto-700",
    color: "rgba(0,51,102,1)",
    height: 19,
    width: 81,
    fontSize: 16,
    marginTop: 8
  },
  group7: {
    width: 87,
    height: 72,
    marginLeft: 107
  },
  icon7: {
    color: "rgba(62,198,255,1)",
    fontSize: 45,
    width: 35,
    height: 49,
    marginLeft: 21
  },
  mukhlisH8: {
    fontFamily: "roboto-700",
    color: "rgba(0,51,102,1)",
    height: 19,
    width: 87,
    fontSize: 16,
    marginTop: 4
  },
  group6Row: {
    height: 72,
    flexDirection: "row",
    marginTop: 15,
    marginLeft: 45,
    marginRight: 39
  },
  group5: {
    width: 200,
    height: 200,
    marginTop: -451,
    alignSelf: "center"
  },
  ellipse: {
    top: 0,
    left: 0,
    width: 200,
    height: 200,
    position: "absolute"
  },
  icon: {
    top: 0,
    left: 42,
    position: "absolute",
    color: "rgba(202,202,202,1)",
    fontSize: 156,
    width: 117,
    height: 171
  },
  ellipseStack: {
    width: 200,
    height: 200
  },
  tentangSaya: {
    fontFamily: "roboto-700",
    color: "#121212",
    fontSize: 36,
    width: 220,
    height: 42,
    marginTop: -242,
    marginLeft: 79
  },
  mukhlisHanafi71: {
    fontFamily: "roboto-700",
    color: "rgba(0,51,102,1)",
    fontSize: 24,
    width: 163,
    height: 28,
    marginTop: 235,
    marginLeft: 106,
    alignSelf: "center"
  },
  reactNativeDeveloper: {
    fontFamily: "roboto-regular",
    color: "rgba(62,198,255,1)",
    fontSize: 16,
    width: 168,
    height: 19,
    marginTop: 8,
    marginLeft: 104
  }
});

export default AboutScreen;
