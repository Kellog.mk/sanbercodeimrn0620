import React, { Component } from "react";
import { StyleSheet, View, Image, Text } from "react-native";

function RegisterScreen(props) {
  return (
    <View style={styles.container}>
      <Image
        source={require("./assets/images/logo.png")}
        resizeMode="contain"
        style={styles.image}
      ></Image>
      <Text style={styles.register}>Register</Text>
      <View style={styles.group}>
        <Text style={styles.username}>Username</Text>
        <View style={styles.rect}></View>
      </View>
      <View style={styles.group2}>
        <Text style={styles.email}>Email</Text>
        <View style={styles.rect2}></View>
      </View>
      <View style={styles.group3}>
        <Text style={styles.email3}>Password</Text>
        <View style={styles.rect3}></View>
      </View>
      <View style={styles.group4}>
        <Text style={styles.ulangiPassword}>Ulangi Password</Text>
        <View style={styles.rect4}></View>
      </View>
      <Text style={styles.atau}>atau</Text>
      <View style={styles.group6}>
        <View style={styles.rect6}>
          <Text style={styles.daftar2}>Daftar</Text>
        </View>
      </View>
      <View style={styles.group7}>
        <View style={styles.rect7}>
          <Text style={styles.masuk}>Masuk ?</Text>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  image: {
    width: 375,
    height: 116,
    marginTop: 59
  },
  register: {
    fontFamily: "roboto-regular",
    color: "#121212",
    height: 28,
    width: 88,
    fontSize: 24,
    marginTop: 60,
    marginLeft: 143
  },
  group: {
    width: 294,
    height: 72,
    marginTop: 39,
    marginLeft: 41
  },
  username: {
    fontFamily: "roboto-regular",
    color: "rgba(0,51,102,1)",
    height: 19,
    width: 73,
    fontSize: 16
  },
  rect: {
    width: 294,
    height: 48,
    backgroundColor: "rgba(255,255,255,1)",
    borderWidth: 1,
    borderColor: "rgba(0,51,102,1)",
    shadowColor: "rgba(0,0,0,1)",
    shadowOffset: {
      width: 3,
      height: 3
    },
    elevation: 5,
    shadowOpacity: 1,
    shadowRadius: 0,
    marginTop: 5
  },
  group2: {
    width: 294,
    height: 71,
    marginTop: 16,
    marginLeft: 40
  },
  email: {
    fontFamily: "roboto-regular",
    color: "rgba(0,51,102,1)",
    height: 19,
    width: 73,
    fontSize: 16
  },
  rect2: {
    width: 294,
    height: 48,
    backgroundColor: "rgba(255,255,255,1)",
    borderWidth: 1,
    borderColor: "rgba(0,51,102,1)",
    shadowColor: "rgba(0,0,0,1)",
    shadowOffset: {
      width: 3,
      height: 3
    },
    elevation: 5,
    shadowOpacity: 1,
    shadowRadius: 0,
    marginTop: 5
  },
  group3: {
    width: 294,
    height: 71,
    marginTop: 16,
    marginLeft: 40
  },
  email3: {
    fontFamily: "roboto-regular",
    color: "rgba(0,51,102,1)",
    height: 19,
    width: 73,
    fontSize: 16
  },
  rect3: {
    width: 294,
    height: 48,
    backgroundColor: "rgba(255,255,255,1)",
    borderWidth: 1,
    borderColor: "rgba(0,51,102,1)",
    shadowColor: "rgba(0,0,0,1)",
    shadowOffset: {
      width: 3,
      height: 3
    },
    elevation: 5,
    shadowOpacity: 1,
    shadowRadius: 0,
    marginTop: 5
  },
  group4: {
    width: 294,
    height: 71,
    marginTop: 16,
    marginLeft: 40
  },
  ulangiPassword: {
    fontFamily: "roboto-regular",
    color: "rgba(0,51,102,1)",
    height: 19,
    width: 120,
    fontSize: 16
  },
  rect4: {
    width: 294,
    height: 48,
    backgroundColor: "rgba(255,255,255,1)",
    borderWidth: 1,
    borderColor: "rgba(0,51,102,1)",
    shadowColor: "rgba(0,0,0,1)",
    shadowOffset: {
      width: 3,
      height: 3
    },
    elevation: 5,
    shadowOpacity: 1,
    shadowRadius: 0,
    marginTop: 5
  },
  atau: {
    fontFamily: "roboto-regular",
    color: "rgba(62,198,255,1)",
    height: 28,
    width: 48,
    fontSize: 24,
    marginTop: 96,
    marginLeft: 163
  },
  group6: {
    width: 140,
    height: 40,
    borderRadius: 16,
    marginTop: -84,
    marginLeft: 117
  },
  rect6: {
    width: 140,
    height: 40,
    backgroundColor: "rgba(0,51,102,1)",
    borderWidth: 1,
    borderColor: "#000000",
    borderRadius: 15
  },
  daftar2: {
    fontFamily: "roboto-regular",
    color: "rgba(255,255,255,1)",
    height: 28,
    width: 72,
    fontSize: 24,
    marginTop: 6,
    marginLeft: 35
  },
  group7: {
    width: 140,
    height: 40,
    borderRadius: 16,
    marginTop: 60,
    marginLeft: 117
  },
  rect7: {
    width: 140,
    height: 40,
    backgroundColor: "rgba(62,198,255,1)",
    borderWidth: 1,
    borderColor: "#000000",
    borderRadius: 15
  },
  masuk: {
    fontFamily: "roboto-regular",
    color: "rgba(255,255,255,1)",
    height: 28,
    width: 90,
    fontSize: 24,
    marginTop: 6,
    marginLeft: 25
  }
});

export default RegisterScreen;
