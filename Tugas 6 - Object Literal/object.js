

function arrayToObject(arr) {
  // Code di sini
  var personObj = {};
  var now = new Date()
  var thisYear = now.getFullYear() // 2020 (tahun sekarang)
  
  if(!arr || arr.length == 0){
    console.log("");
  }
  for (var i = 0; i < arr.length; i++){
    personObj.firstname = arr[i][0];
    personObj.lastName  = arr[i][1];
    personObj.gender  = arr[i][2]; 
    if (!arr[i][3] || arr[i][3] > thisYear){
      personObj.age = "Invalid birth year";
    } else {
      personObj.age = thisYear - arr[i][3];
    }
    
    console.log(`${i+1}. ${personObj.firstname} ${personObj.lastName} :`, personObj);
  }
}

// Driver Code
var people = [["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"]]
arrayToObject(people)
/*
1. Bruce Banner: {
firstName: "Bruce",
lastName: "Banner",
gender: "male",
age: 45
}
2. Natasha Romanoff: {
firstName: "Natasha",
lastName: "Romanoff",
gender: "female".
age: "Invalid Birth Year"
}
*/
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2)
/*
1. Tony Stark: {
firstName: "Tony",
lastName: "Stark",
gender: "male",
age: 40
}
2. Pepper Pots: {
firstName: "Pepper",
lastName: "Pots",
gender: "female".
age: "Invalid Birth Year"
}
*/
// Error case
arrayToObject([]) // ""


function shoppingTime(memberId, money) { //test

  listPurchased=[]
  changeMoney= 0
  totalBelanja = 0
  listItem = [
      ['Sepatu Stacattu', 1500000],
      ['Baju Zoro', 500000],
      ['Baju H&N', 250000],
      ['Sweater Uniklooh', 175000],
      ['Casing Handphone', 50000]
  ]



  for(var i = 0; i < listItem.length; i++){

      if(memberId == undefined || memberId == ''){
          return 'Mohon maaf, toko X hanya berlaku untuk member saja'
      }else if(money < 50000){
          return 'Mohon maaf, uang tidak cukup'
      }else if(money > listItem[i][1]){
          listPurchased.push(listItem[i][0])
          totalBelanja += listItem[i][1]
          changeMoney = money - totalBelanja
      }
  }


  var obj = {
      memberId: memberId,
      money: money,
      listPurchased: listPurchased,
      changeMoney: changeMoney
  }
  

  return obj
}

// TEST CASES
console.log(shoppingTime('1820RzKrnWn08', 2475000));
  //{ memberId: '1820RzKrnWn08',
  // money: 2475000,
  // listPurchased:
  //  [ 'Sepatu Stacattu',
  //    'Baju Zoro',
  //    'Baju H&N',
  //    'Sweater Uniklooh',
  //    'Casing Handphone' ],
  // changeMoney: 0 }
console.log(shoppingTime('82Ku8Ma742', 170000));
//{ memberId: '82Ku8Ma742',
// money: 170000,
// listPurchased:
//  [ 'Casing Handphone' ],
// changeMoney: 120000 }
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

function naikAngkot(arrPenumpang) {
  var result = []
  rute = ['A', 'B', 'C', 'D', 'E', 'F'];

  var findNaikDari = 0
  for(var i = 0; i < arrPenumpang.length; i++){
      for(var j = 0; j < rute.length; j++){
          if(arrPenumpang[i][1] == rute[j]){
              findNaikDari = j
          }
      }

      var counter=0
      for(var k = findNaikDari+1; k < rute.length; k++ ){
          counter++
          if(arrPenumpang[i][2] == rute[k]){
              break
          }
      }

      var obj = {
          penumpang: arrPenumpang[i][0],
          naikDari: arrPenumpang[i][1],
          tujuan: arrPenumpang[i][2],
          bayar: counter * 2000
      }

      result.push(obj)
  }


  return result
}

//TEST CASE
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
// [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
//   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]

  console.log(naikAngkot([])); //[]
