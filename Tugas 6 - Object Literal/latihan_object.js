var personArr = ["John", "Doe", "male", 27]
var personObj = {
firstName : "John",
lastName: "Doe",
gender: "male",
age: 27
}

console.log(personArr) // John
console.log(personObj) // John

console.log(personArr[0]) // John
console.log(personObj.firstName) // John

var motorcycle1 = {
    brand: "Honda",
    type: "CUB",
    price: 1000
    }

console.log(motorcycle1.brand) // "Handa"
console.log(motorcycle1.type) // "CUB"

var array = [1, 2, 3]
console.log(typeof array) // object
