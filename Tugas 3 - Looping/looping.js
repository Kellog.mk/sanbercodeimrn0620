// No 1. Looping While
var flag = 1;

console.log("LOOPING PERTAMA");

while (flag < 11){
 console.log(flag*2 + ' - I love coding');
 flag++;
}

console.log("LOOPING KEDUA");
flag--;
while (flag > 0){
    console.log(flag*2 + ' - I will become a mobile developer');
    flag--;
   }

console.log(" ");
   
// No. 2 Looping menggunakan for
flag = 1;
for(flag = 1; flag < 21; flag++){
 if (flag % 2 == 0) { // even
    console.log(flag + ' - Berkualitas');
 } else { // odd
    if (flag % 3 == 0){
     console.log(flag + ' - I Love Coding');
    } else {
      console.log(flag + ' - Santai'); 
    }
 }
}

console.log(" ");

flag = 1;

// No. 3 Membuat Persegi Panjang #
var output = [];

    for (var i = 0; i < 8; i++) {
        output.push('#');
    }

    for (flag = 0; flag < 4; flag++){
     console.log(output.join(''));       
    }
        
console.log(" ");

//No. 4 Membuat Tangga
output = [];
for (flag = 0; flag < 7; flag++){
    for (var i = 0; i < 1; i++) {
        output.push('#');
    }
    console.log(output.join(''));    
}

console.log(" ");

//No. 5 Membuat Papan Catur

const size = 8;
for (let i = 1; i <= size; i++) {
    let line = "";
    for (let j = 1; j <= size; j++) {
        (j + i) % 2 === 0 ?line += " " : line += "#";
    }
    console.log(line);
}

