// di index.js
var readBooks = require('./callback.js')
var books = [
    {name: 'LOTR', timeSpent: 3000},
    {name: 'Fidas', timeSpent: 2000},
    {name: 'Kalkulus', timeSpent: 4000}
]

// function readBooks(time, book, callback ) {
//     console.log(`saya membaca ${book.name}`)
//     setTimeout(function(){
//         let sisaWaktu = 0
//         if(time > book.timeSpent) {
//             sisaWaktu = time - book.timeSpent
//             console.log(`saya sudah membaca ${book.name}, sisa waktu saya ${sisaWaktu}`)
//             callback(sisaWaktu) //menjalankan function callback
//         } else {
//             console.log('waktu saya habis')
//             callback(time)
//         }
//     }, book.timeSpent)
// }
    
//     module.exports = readBooks


// Tulis code untuk memanggil function readBooks di sini

var i = 0;
var timeLimit = 10000;
var numOfBooks = books.length;

readBooks(timeLimit, books[i], function call(sisaWaktu){
    if(sisaWaktu != timeLimit && ++i < numOfBooks){
        timeLimit = sisaWaktu;
        readBooks(timeLimit, books[i], call);
    }

})