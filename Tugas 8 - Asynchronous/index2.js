var readBooksPromise = require('./promise.js')
var books = [
    {name: 'LOTR', timeSpent: 3000},
    {name: 'Fidas', timeSpent: 2000},
    {name: 'Kalkulus', timeSpent: 4000}
]

// // di file promise.js
// function readBooksPromise (time, book) {
//     console.log(`saya mulai membaca ${book.name}`)
//     return new Promise( function (resolve, reject){
//         setTimeout(function(){
//             let sisaWaktu = time - book.timeSpent
//             if(sisaWaktu >= 0 ){
//                 console.log(`saya sudah selesai membaca ${book.name}, sisa waktu saya
//                 ${sisaWaktu}`)
//                 resolve(sisaWaktu)
//             } else {
//                 console.log(`saya sudah tidak punya waktu untuk baca ${book.name}`)
//                 reject(sisaWaktu)
//             }
//         }, book.timeSpent)
//     })
// }
//     module.exports = readBooksPromise

// Lanjutkan code untuk menjalankan function readBooksPromise
var i = 0;
var timeLimit = 10000;
var numOfBooks = books.length;
var catchError = function(error) {};

readBooksPromise(timeLimit, books[i])
.then(function call(sisaWaktu){
    if(sisaWaktu != timeLimit && ++i < numOfBooks){
        timeLimit = sisaWaktu;
        readBooksPromise(timeLimit,books[i])
        .then(call)
        .catch(catchError);
    }

})
.catch(catchError);